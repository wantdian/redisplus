package com.hong.springboot.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootDemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	DemoService demoService;

	@Test
	public void multithreadingTest()throws Exception{
		ExecutorService executorService= Executors.newFixedThreadPool(100);
		Runnable task= new Runnable() {
			public void run() {
				try {
					String result=demoService.testService("sleep");
					System.err.println("线程:["+Thread.currentThread().getName()+"]拿到结果=》"+result);
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		};
		for(int i = 0; i<100;i++){
			executorService.submit(task);
		}
		System.in.read();
	}
}
